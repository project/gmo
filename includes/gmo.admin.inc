<?php

/**
 * @file
 * Admin page callback file for the gmo module.
 */

/**
 * Returns form items for the gmo api settings page.
 *
 * Menu callback for admin/config/services/gmo.
 *
 * @return array
 *   A form for the gmo api settings page.
 */
function gmo_settings($form, &$form_state) {
  // Make sure GMO SDK installed.
  // If not, display messages and disable API settings.
  list($status, $version, $error_message) = gmo_sdk_check(TRUE);
  $disabled = ($status != GMO_SDK_LOADED);

  if ($status == GMO_SDK_NOT_LOADED) {
    drupal_set_message(t('Please make sure the GMO Payment Gateway SDK library is installed in the libraries directory.'), 'error');
    if ($error_message) {
      drupal_set_message($error_message, 'error');
    }
  }
  elseif ($status == GMO_SDK_OLD_VERSION) {
    drupal_set_message(
      t(
        'Please make sure the GMO Payment Gateway SDK library installed is @version or greater. Current version is @current_version.',
        array(
          '@version' => GMO_SDK_MINIMUM_VERSION,
          '@current_version' => $version,
        )
      ),
      'warning'
    );
  }

  // Build API settings form.
  $form = array();

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('API Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => $disabled,
    '#description' => t('In order to check the validity of the API, system will be test connect to GMO Payment Gateway after updated.'),
  );

  $form['settings']['gmo_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Host'),
    '#required' => TRUE,
    '#default_value' => variable_get('gmo_host', ''),
    '#description' => t('Host of GMO Payment Gateway. Example: [@url].', array('@url' => 'https://pt01.mul-pay.jp/payment/')),
    '#disabled' => $disabled,
  );

  $form['settings']['gmo_site_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Site id'),
    '#required' => TRUE,
    '#default_value' => variable_get('gmo_site_id', ''),
    '#description' => t('Site id of GMO Payment Gateway. Example: [@site_id].', array('@site_id' => 'tsite00000000')),
    '#maxlength' => 13,
    '#disabled' => $disabled,
  );

  $form['settings']['gmo_site_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Site pass'),
    '#required' => TRUE,
    '#default_value' => variable_get('gmo_site_pass', ''),
    '#description' => t('Site pass of GMO Payment Gateway.'),
    '#maxlength' => 20,
    '#disabled' => $disabled,
  );

  $form['settings']['gmo_shop_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Shop id'),
    '#required' => TRUE,
    '#default_value' => variable_get('gmo_shop_id', ''),
    '#description' => t('Shop id of GMO Payment Gateway. Example: [@shop_id].', array('@shop_id' => 'tshop00000000')),
    '#maxlength' => 13,
    '#disabled' => $disabled,
  );

  $form['settings']['gmo_shop_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Shop pass'),
    '#required' => TRUE,
    '#default_value' => variable_get('gmo_shop_pass', ''),
    '#description' => t('Shop pass of GMO Payment Gateway.'),
    '#maxlength' => 10,
    '#disabled' => $disabled,
  );

  $form['#validate'][] = 'gmo_settings_validate';

  return system_settings_form($form);
}

/**
 * Validator for the gmo_settings() form.
 */
function gmo_settings_validate($form, &$form_state) {
  if (form_get_errors()) {
    return;
  }
  // Current api settings.
  $keys = gmo_config_load();
  // New api settings.
  $new_keys = array(
    'host'      => $form_state['values']['gmo_host'],
    'site_id'   => $form_state['values']['gmo_site_id'],
    'site_pass' => $form_state['values']['gmo_site_pass'],
    'shop_id'   => $form_state['values']['gmo_shop_id'],
    'shop_pass' => $form_state['values']['gmo_shop_pass'],
  );
  // Return if no changes.
  if ($keys == $new_keys) {
    return;
  }
  // Validate the new api settings.
  // E01030002 - Invalid shop id or shop pass.
  // E01210002 - Invalid site id or site pass.
  try {
    $api = new GMODrupalPaymentShopAndSiteApi($new_keys);
    $data = $api->tradedCard(0, 0);
    $response = $data['response'];
    if (preg_match('/E01(21|03)0002/', $response)) {
      form_set_error('', t('Invalid API info, please check your settings.'));
    }
  }
  catch (Exception $e) {
    form_set_error('', $e->getMessage());
  }
}
