<?php

/**
 * @file
 * API class implememnt on Drupal with configurable variables.
 */

libraries_load('gmo-pg-php');

/**
 * Implement ShopApi.
 */
class GMODrupalPaymentShopApi extends GMO\Payment\ShopApi {

  /**
   * Object constructor.
   */
  public function __construct($c = array(), $params = array()) {
    if (empty($c)) {
      $c = gmo_config_load();
    }

    parent::__construct($c['host'], $c['shop_id'], $c['shop_pass'], $params);
  }

}

/**
 * Implement SiteApi.
 */
class GMODrupalPaymentSiteApi extends GMO\Payment\SiteApi {

  /**
   * Object constructor.
   */
  public function __construct($c = array(), $params = array()) {
    if (empty($c)) {
      $c = gmo_config_load();
    }

    parent::__construct($c['host'], $c['site_id'], $c['site_pass'], $params);
  }

}

/**
 * Implement ShopAndSiteApi.
 */
class GMODrupalPaymentShopAndSiteApi extends GMO\Payment\ShopAndSiteApi {

  /**
   * Object constructor.
   */
  public function __construct($c = array(), $params = array()) {
    if (empty($c)) {
      $c = gmo_config_load();
    }

    parent::__construct($c['host'], $c['shop_id'], $c['shop_pass'], $c['site_id'], $c['site_pass'], $params);
  }

}

/**
 * Implement ShopApi.
 */
class GMODrupalPaymentConsts extends GMO\Payment\Consts {

}
