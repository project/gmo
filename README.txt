This module is an integration between the GMO Payment Gateway
 and Drupal commerce sites.

It allows Drupal Commerce or Ubercart to process payments
 using the GMO Payment Gateway.

Requirements
============

GMO Payment Gateway SDK for PHP - https://github.com/everright/gmo-pg-php

Usage
=====

- Install and enable module 'gmo' as usual.
- Go to GMO setting page, save your API info of GMO Payment Gateway.
- Enable "gmo_commerce" module if your site implement by Drupal Commerce.
- Enable "gmo_ubercart" module if your site implement by Ubercart.
- Start your payment with GMO and enjoy!

Bugs
====

Please report bugs and issues on drupal.org in the issue queue:
http://drupal.org/project/issues/gmo

Remember to first search the issue queue and help others where you can!

Credits
=======

The module was built by everright in Ci&T (http://www.ciandt.com).
